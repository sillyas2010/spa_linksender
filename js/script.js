var icon ="";
var att_class="";
var index = 0;
var att_classes =new Array(5);

$("input:checkbox").bind("change", function () { 
    if (chbStatus(this)){
      createAtt(this); 
    }
    else {
        deleteAttSameClass(this);
        selAll_verify(this);
    }
    
 
});

$(document).on('click', '.del_btn', function att_del(del_btn) {
    var att_el= $(this).parent();
    att_classes=att_el.attr("class").split(" ");
    uncheckSameClass(att_classes[1]);
    att_el.remove();
    attValueChanged();
});

   /* function getClasses(element) {
        return element.attr("class").split(" ");
    }
*/
    function deleteAttSameClass(element) {
        var checkbox_classes=element.attr("class").split(" ");
        /*var same_class= att_classes[1];
        var attach= $('.del_btn'+same_class);
        $(attach).parent().remove();*/
        attValueChanged();
    }


    function uncheckSameClass(same_class) {
        var checkbox =$('.filled-in.'+same_class);
                    checkbox[0].checked=false;
                    selAll_verify(checkbox[0]);
                    checkbox.removeClass(same_class);
    }

    function chbStatus(elem) {
        if (elem.checked)
             return true;
        else return false;
    }
    
    function attValueChanged() {
        $('#num_of_links').text($('.att_list').children().length + " attached links");
    }

    function selAll_verify(checkbox) {
        var f = checkbox.form; 
        var arr = [].slice.call(f); 
        var link = $(f).parent().next().find(".select_all");
        
        if(arr.every(function (elem) {
                return elem.checked == true;
            })) { link.html("REMOVE ALL"); } 
        else    { link.html("ADD ALL"); } 
    }


    function addIndexClass() {
              index++;
              att_class="attItem_" + index;
              return att_class;
            }


    function createAtt (checkbox){
        $(checkbox).addClass(addIndexClass);
        var lbl_text = $(checkbox).parent().find(".info_contain").text();
        var node = $('<li />', {
                    "class": 'att_el '+att_class});
        attSectionIcon(checkbox);
        var span = $(('<span />'), {
                    "class": 'att_desc'});
        var del_btn = $(('<a />'), {
                    "class": 'del_btn'});
        span.append(lbl_text);
        node.append(icon,span,del_btn);
        var att =  $(checkbox).parent().parent().parent().parent().parent().next().find(".att_list");
        att.append(node);
        attValueChanged();
    }


    function sel(form,button) {
        var checks = $(form).find(".filled-in");
        for (var i = 0; i < checks.length; i++) {
            var check = checks[i];
           if($(button).html()=="ADD ALL") {
               if(chbStatus(check)==false)
                   {
                       createAtt (check);
                   }
               check.checked = true;
           }
           else
            check.checked = false;
        }
        switchStatus($(button));
    }
    
    function attSectionIcon(checkbox) {
        if($(checkbox).hasClass('doc_chb')) {
                icon = $('<div />', {
                    "class": 'att_ic ic_doc'});
        }
        else if($(checkbox).hasClass('links_chb')) {
                icon = $('<div />', {
                    "class": 'att_ic ic_links'});
        }
        else if($(checkbox).hasClass('sc_chb')) {
                icon = $('<div />', {
                    "class": 'att_ic ic_sc'});
        }
        
        return icon;
    }

    function email_send() {
        alert("Thank you for message! Your name is: "+ $('#f_name').val()+ " ; Your email is: "+ $('#f_email').val()+ " ; Your message is: "+ $('#f_msg').val()+ " ; And attached: "+ $('.att_list').children().length + " links to us!");
        
        $('#f_name').val("");
        $('#f_email').val("");
        $('#f_msg').val("");
        $('.att_list').children().remove();
        attValueChanged();
    }

    function switchStatus(elem) {
        if($(elem).html()=="ADD ALL") {$(elem).html("REMOVE ALL");}
        else {$(elem).html("ADD ALL");}
    }